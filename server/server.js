
var drawingServer = false;

const express = require('express');
var app = express();
var routes = require('./routes/routes.js');
app.use('/', express.static(__dirname + '/../client')); // redirect root
var http = require('http');
var colors = require('colors');
var dateFormat = require('dateformat');
//var bodyParser =  require('body-parser');
//Set express to use bodyParser to read form submissions
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
var cookieSession = require('cookie-session');
var requestUtil = require('request');
var axidrawLocation = "http://127.0.0.1:4242/robopaint/v1/print";
var serverLocation = "http://arthackday.herokuapp.com/drawStatus";


var fs = require('fs'); //We need to be able to read and write the file system
var path = require('path'); //We need to be able to handle paths
var now = require('moment'); //Use moment to time stamp files

var multiparty = require('multiparty');

//Connect to mongodb
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://heroku_vxcwlccs:m96sv83dra4pgrosr1m5hn9tto@ds143000.mlab.com:43000/heroku_vxcwlccs';

var db;
var dbParts;
var dbSessionParts;
var dbUsers;

var currentSessionBodies = [];
var currentSessionEyes = [];
var currentSessionMouths = [];
var currentSessionNoses = [];

var fakeIt = true;
// Use connect method to connect to the Server
MongoClient.connect(url, function (err, connecteddb) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err);
  } else {
    //HURRAY!! We are connected. :)
    console.log('Connection established to', url);

    db = connecteddb;
    dbParts = db.collection('parts');
    dbSessionParts = db.collection('sessionParts');
    dbUsers = db.collection('users');
  }
});


var scale = 1.35;

//var scale = 3;

var numOfBodies = 4;
var numOfEyes = 4;
var numOfNoses = 4;
var numOfMouths = 4;
var numOfNames = 31;
var numOfQuotes = 22;
var numOfPlayers = 9;

var currentSession = 112;
var currentNumberOfPlayers = 0;

app.use(cookieSession({
  name: 'session',
  secret: 'supersecretthingtosaltWith2',
  maxAge: 24 * 60 * 60 * 1000
}));


if (!fs.existsSync('renders')){
  console.log("Renders folder doesn't exist. Creating");
  fs.mkdirSync('renders');
}

app.listen(process.env.PORT || 3000, function () {
  console.log('Example app listening on port 3000!');
})

var drawSVG = function(string){
    var requestStructure = {
                                "options": {
                                    "name": "Monster",
                                    "noresize": true
                                },
                                "svg": string
                            }
    requestUtil({
      url: axidrawLocation,
      method: "POST",
      json: true,
      headers: {
          "content-type": "application/json",
      },
      body: requestStructure
    }).on('response', function(response) {
    console.log(response.statusCode) // 200
    console.log(response) // 'image/png'
  });
}

var printMonster = function(body, eyes, nose, mouth){
  var paper = require('paper');
  var canvas = new paper.Canvas(750, 1350, 'pdf');
  paper.setup(canvas);

  //Using paper, render the PDF file
  with(paper){
    var contents = fs.readFileSync('server/assets/bodies/body' + body + '.svg').toString();
    var loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    contents = fs.readFileSync('server/assets/eyes/eye' + eyes + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    contents = fs.readFileSync('server/assets/noses/nose' + nose + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    contents = fs.readFileSync('server/assets/mouths/mouth' + mouth + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    var quoteNumber = getRandomIntInclusive(1,numOfQuotes);
    quoteNumber = pad(quoteNumber,2);
    contents = fs.readFileSync('server/assets/quotes/quote' + quoteNumber + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    var nameNumber = pad(getRandomIntInclusive(1,numOfNames),2);
    contents = fs.readFileSync('server/assets/names/name' + nameNumber + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    var playerNumber = getRandomIntInclusive(1,currentNumberOfPlayers);
    if(playerNumber == 0) playerNumber = getRandomIntInclusive(1,9);
    contents = fs.readFileSync('server/assets/players/player' + playerNumber + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    var file = paper.project.exportSVG({asString : true});
    drawSVG(file);
  }
}

//Create a copy of the 10Print file
app.get('/printMonster', function (request, response) {
  console.log("Printing Monster");


  var paper = require('paper');
  var canvas = new paper.Canvas(750, 1350, 'pdf');
  paper.setup(canvas);

  //Using paper, render the PDF file
  with(paper){

     var contents = fs.readFileSync('server/assets/bodies/body1.svg').toString();
    var loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

    contents = fs.readFileSync('server/assets/eyes/eye4.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

     contents = fs.readFileSync('server/assets/mouths/mouth4.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));

     contents = fs.readFileSync('server/assets/noses/nose1.svg').toString();
     loadedShape = paper.project.importSVG(contents);
     loadedShape.scale(scale, new Point(0, 0));

    var file = paper.project.exportSVG({asString : true});
    drawSVG(file);
  }
  response.setHeader('Content-Type', 'text/html');
        response.write("ok");
        response.end();
});

// app.get('/printRandomMonster', function (request, response) {
//   console.log("Rendering 10Print.pdf");

//   var paper = require('paper');
//   var canvas = new paper.Canvas(750, 1350, 'pdf');
//   paper.setup(canvas);

//   //Using paper, render the PDF file
//   with(paper){
//     var contents = fs.readFileSync('server/assets/bodies/body' + getRandomIntInclusive(1,numOfBodies) + '.svg').toString();
//     var loadedShape = paper.project.importSVG(contents);
//     loadedShape.scale(scale, new Point(0, 0));

//     contents = fs.readFileSync('server/assets/eyes/eye' + getRandomIntInclusive(1,numOfEyes) + '.svg').toString();
//     loadedShape = paper.project.importSVG(contents);
//     loadedShape.scale(scale, new Point(0, 0));

//     contents = fs.readFileSync('server/assets/noses/nose' + getRandomIntInclusive(1,numOfNoses) + '.svg').toString();
//     loadedShape = paper.project.importSVG(contents);
//     loadedShape.scale(scale, new Point(0, 0));

//     contents = fs.readFileSync('server/assets/mouths/mouth' + getRandomIntInclusive(1,numOfMouths) + '.svg').toString();
//     loadedShape = paper.project.importSVG(contents);
//     loadedShape.scale(scale, new Point(0, 0));

//     console.log("Randomizing number");
//     var quoteNumber = getRandomIntInclusive(1,numOfQuotes);
//     console.log("Adding Padding");
//     quoteNumber = pad(quoteNumber,2);
//     contents = fs.readFileSync('server/assets/quotes/quote' + quoteNumber + '.svg').toString();
//     loadedShape = paper.project.importSVG(contents);
//     loadedShape.scale(scale, new Point(0, 0));

//     // var nameNumber = pad(getRandomIntInclusive(1,numOfNames),2);
//     // contents = fs.readFileSync('server/assets/names/name' + getRandomIntInclusive(1,nameNumber) + '.svg').toString();
//     // loadedShape = paper.project.importSVG(contents);
//     // loadedShape.scale(scale, new Point(0, 0));

//     // var playerNumber = getRandomIntInclusive(1,numOfPlayers);
//     // contents = fs.readFileSync('server/assets/players/player' + getRandomIntInclusive(1,playerNumber) + '.svg').toString();
//     // loadedShape = paper.project.importSVG(contents);
//     // loadedShape.scale(scale, new Point(0, 0));

//     var file = paper.project.exportSVG({asString : true});
//     drawSVG(file);
//   }

//   response.setHeader('Content-Type', 'text/html');
//         response.write("ok");
//         response.end();
// });

app.get('/printRandomMonster', function (request, response) {
  PrintRandomMonster();
  response.status(200).send("ok");
});

app.get('/downloadRandomMonster', function (request, response) {
  console.log("Rendering YourMonster.pdf");

  if (request.session.count == null) {
      request.session.count = 0;
  }
  request.session.count++;


  var paper = require('paper');
  var canvas = paper.Canvas(180 * scale, 324 * scale, 'pdf');
  paper.setup(canvas);

  //Using paper, render the PDF file
  with(paper){
    var contents = fs.readFileSync('server/assets/bodies/body' + getRandomIntInclusive(1,numOfBodies) + '.svg').toString();
    var loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1; 

    contents = fs.readFileSync('server/assets/eyes/eye' + getRandomIntInclusive(1,numOfEyes) + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1; 

    contents = fs.readFileSync('server/assets/noses/nose' + getRandomIntInclusive(1,numOfNoses) + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1; 

    contents = fs.readFileSync('server/assets/mouths/mouth' + getRandomIntInclusive(1,numOfMouths) + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1; 

    console.log("Randomizing number");
    var quoteNumber = getRandomIntInclusive(1,numOfQuotes);
    console.log("Adding Padding");
    quoteNumber = pad(quoteNumber,2);
    contents = fs.readFileSync('server/assets/quotes/quote' + quoteNumber + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1; 

    var nameNumber = pad(getRandomIntInclusive(1,numOfNames),2);
    contents = fs.readFileSync('server/assets/names/name' + nameNumber + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1;

    var playerNumber = getRandomIntInclusive(1,numOfPlayers);
    contents = fs.readFileSync('server/assets/players/player' + playerNumber + '.svg').toString();
    loadedShape = paper.project.importSVG(contents);
    loadedShape.scale(scale, new Point(0, 0));
    loadedShape.strokeWidth = 1;

    view.update();
    var thisMoment = now();
    fs.writeFile("renders/" + thisMoment + '.pdf', canvas.toBuffer(), function (err) {
      if (err)
          throw err;
      var newfile = fs.readFileSync("renders/" + thisMoment + '.pdf');
      var filePath = "renders/" + thisMoment + '.pdf';
      response.download(filePath, 'YourMonster.pdf', function(err) {
        if (!err) {
            fs.unlink(filePath);
        }
      });
      console.log('Saved!');
    });
    
  }
});


app.post("/endSession", function (request, response) {
  currentSession++;
  response.status(200).send("ok");
});

app.post("*",function (request, response) {
  if ((request.session.sessionNumber == null) || (request.session.sessionNumber != currentSession)) {
      console.log("Adding to session count.");
      console.log("Session number:" + request.session.sessionNumber);
      console.log("Player number:" + request.session.playerNmber);
      request.session.sessionNumber = currentSession;
      currentNumberOfPlayers++;
      request.session.playerNumber = currentNumberOfPlayers;
      AddUser();

      var form = new multiparty.Form();

      form.parse(request, function(err, fields, files) {
        console.log("\nBody: " + fields['body'] + "\nEyes: " + fields['eyes'] + "\nNose: " + fields['nose'] + "\nMouth: " + fields['mouth']);
        AddParts(fields['body'], fields['eyes'], fields['nose'], fields['mouth']);
      });
  }
  else{
    console.log("User already submitted this round");
  }

  response.writeHead(200, {"Content-Type": "application/json"});

  var responseContent = { number:request.session.playerNumber};
  var json = JSON.stringify(responseContent);
  response.end(json);
});

var readyToDraw = true;
app.get("/drawStatus", function (request, response) {
  if(readyToDraw){
    responseContent.status(200);
  }
  else{
    response.staus(202).send("Not ready");
  }
});

/***********
UTILITY URLS
************/
app.get("/showCurrentParts", function (request, response) {
    response.status(200);
    console.log("Bodies:\n");
    for(var i = 0; i < currentSessionBodies.length; i++){
      console.log("\t Body: " + currentSessionBodies[i].part + ", " + currentSessionBodies[i].count);
    }
    console.log("Eyes:\n");
    for(var i = 0; i < currentSessionEyes.length; i++){
      console.log("\t Eyes: " + currentSessionEyes[i].part + ", " + currentSessionEyes[i].count);
    }
    console.log("Noses:\n");
    for(var i = 0; i < currentSessionNoses.length; i++){
      console.log("\t Noses: " + currentSessionNoses[i].part + ", " + currentSessionNoses[i].count);
    }
    console.log("Mouths:\n");
    for(var i = 0; i < currentSessionMouths.length; i++){
      console.log("\t Mouths: " + currentSessionMouths[i].part + ", " + currentSessionMouths[i].count);
    }
    response.end();
});

app.get("/setReadyToDraw", function (request, response) {
    response.status(200);
    response.send("ok");
});

app.get("/fakeIt", function (request, response) {
    response.status(200);
    response.send("Ok, Faking it.");
    fakeIt = true;
});

app.get("/dontFakeIt", function (request, response) {
    response.status(200);
    response.send("Ok, I wont fake it.");
    fakeIt = false;
});

/***************************************
FUNCTIONS CALLED IN RESPONSE TO REQUESTS
****************************************/
var AddParts = function(body, eyes, nose, mouth){
  if(currentSessionBodies.length = 0){
    currentSessionBodies.push({part:body, count:1});
  }
  else{
    var found = false;
    for(var i = 0; i < currentSessionBodies.length; i++){
      if(currentSessionBodies[i].part == body){
        currentSessionBodies[i].count++;
      }
    }

    if(!found){
      currentSessionBodies.push({part:body, count:1});
    }
  }

  if(currentSessionEyes.length = 0){
    currentSessionEyes.push({part:eyes, count:1});
  }
  else{
    var found = false;
    for(var i = 0; i < currentSessionEyes.length; i++){
      if(currentSessionEyes[i].part == eyes){
        currentSessionEyes[i].count++;
      }
    }

    if(!found){
      currentSessionEyes.push({part:eyes, count:1});
    }
  }

  if(currentSessionNoses.length = 0){
    currentSessionNoses.push({part:nose, count:1});
  }
  else{
    var found = false;
    for(var i = 0; i < currentSessionNoses.length; i++){
      if(currentSessionNose[i].part == nose){
        currentSessionNose[i].count++;
      }
    }

    if(!found){
      currentSessionNoses.push({part:nose, count:1});
    }
  }

  if(currentSessionMouths.length = 0){
    currentSessionMouths.push({part:mouth, count:1});
  }
  else{
    var found = false;
    for(var i = 0; i < currentSessionMouths.length; i++){
      if(currentSessionMouths[i].part == mouth){
        currentSessionMouths[i].count++;
      }
    }

    if(!found){
      currentSessionMouths.push({part:mouth, count:1});
    }
  }

  AddPartToDB({name:"body", number:body});
  AddPartToDB({name:"eyes", number:eyes});
  AddPartToDB({name:"nose", number:nose});
  AddPartToDB({name:"mouth", number:mouth});
}

var PrintRandomMonster = function(){
  var body = getRandomIntInclusive(1,numOfBodies);
  var eyes = getRandomIntInclusive(1,numOfEyes);
  var nose = getRandomIntInclusive(1,numOfNoses);
  var mouth = getRandomIntInclusive(1,numOfMouths);

  printMonster(body, eyes, nose, mouth);
}

/******************
DATABASE FUNCTIONS
******************/
var AddPartToDB = function(part){
  dbParts.insert(part,function(err, saved) {
    //currentSessionID = saved.insertedIds[0];
    if( err || !saved ){
      console.log("Part not added");
    }
    else{
        console.log("Part added.");
    }
  });
}

var AddUser = function(){
  var currentTime = new Date();
  var newUser = {time:dateFormat(currentTime, "mm/dd/yyyy hh:MM:ss TT")};

  dbUsers.insert(newUser,function(err, saved) {
    //currentSessionID = saved.insertedIds[0];
    if( err || !saved ){
      console.log("User not added");
    }
    else{
        console.log("User added.");
    }
  });
}

/*****************
UTILITY FUNCTIONS
*****************/
var pad = function(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    console.log("Returning " + s + " from " + num);
    return s;
}

var getRandomIntInclusive = function(min, max) {
  console.log("Randomizing number between " + min + " and " + max);
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}



