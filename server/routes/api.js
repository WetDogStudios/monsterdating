var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../models/User');

// middleware to use for all API requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    console.log(req.body);
    next(); // make sure we go to the next routes and don't stop here
});

router.get('/', requireRole(['admin']), function(req, res) {
    res.json({
    	success: true,
    	message: 'Hooray! You talked to the API!' });   
});

router.get('/users', function(req, res) {
	User.find({}, function(err, users) {
    var userMap = {};

    users.forEach(function(user) {
      userMap[user._id] = user;
    });

    res.json(userMap);  
  });  
});

router.route('/setup/createadmin')
	.post(function(req, res){
		//Application has been initialized, deny access
		if(req.app.get('initialized')){
			res.json({
				success: false,
				message: "Setup has already been completed"
			});
		}
		else {

			var userInfo = req.body;
				userInfo.role = 'admin';
			var pwd = req.body.password;
				//delete userInfo.password;
				console.log(pwd)

			 	//userInfo.name = 'admin';
			User.register(new User(userInfo), pwd, function(err, account) {
			    if (err) {
			        //return res.render('register', { account : account });
			        console.log(err);
			        res.json({success: false, message: err.message});
			    }
			    else{
			        passport.authenticate('local')(req, res, function () {
			          //res.redirect('/');
			          res.json({success: true, message: "created and logged in"});
			          req.app.set('initialized', true);
			        });
		    	}
		    });
		}
	});

router.route('/setup/createaccount')
	.post(function(req, res){
		var userInfo = req.body;
			userInfo.role = 'user';
		var pwd = req.body.password;
			//delete userInfo.password;
			console.log(pwd)

		User.register(new User(userInfo), pwd, function(err, account) {
		    if (err) {
		        //return res.render('register', { account : account });
		        console.log(err);
		        res.json({success: false, message: err.message});
		    }
		    else{
		        passport.authenticate('local')(req, res, function () {
		          //res.redirect('/');
		          res.json({success: true, message: "created and logged in"});
		          req.app.set('initialized', true);
		        });
	    	}
	    });
	});

router.route('/login')
	.get(function(req,res){
		console.log(req.user);
		if (req.user) {
			res.json({success: true, message: 'Already logged in', username: req.user.username});
			return;
		}

		res.json({success: false, message: 'No user logged in'});

	})
	.post(function(req,res){

		User.authenticate()(req.body.username, req.body.password, function (error, user, options) {
			if(options){
				if(options.name == 'IncorrectUsernameError'){
					res.json({success: false, message: 'User not found'});
				}
				else{
					res.json({success: false, message: 'Incorrect password'});
				}
				return;
			}

			req.login(user, function(error){
				if(error) res.json({success: false, message: error.message});
				return;
			});
	        res.json({success: true, message: 'Logged in', username: user.username});
	          
	    });

		// User.findOne({'username':req.body.username}, function(err,user){
		// 	if(err) res.json({'success': false, 'message': err.message});

		// 	req.session.username = user.username;
		// 	req.session.role = user.role;

		// 	req.login('wildparadox', function(err) {
		// 		console.log(err);
		// 	  if (err) {
			  	
		// 	  	req.json({success: false, message: err.message});
		// 	  	return;
		// 	  }

		// 	  res.json({success: true, message: 'logged in', username: user.username});
		// 	});
			
	 //    });
	});

router.route('/dev/reset/users')
	.get(function(req,res){
		req.app.get('db').db.dropCollection('users',function(err,result){
			if(err){
				console.log(err);
				res.json({
					success: false,
					message:"Error clearing users collection"});
			}
			else{
				console.log('Users collection cleared');
				res.json({
					success: true,
					message:"Users cleared"});
				req.app.set('initialized',false);
			}
		});
	});

router.route('/dev/reset/session')
	.get(function(req,res){
		if (req.session && req.session.username) {
			req.session.reset();
			res.json({
					success: true,
					message:"Session cleared"
			});
			return;
		}

		res.json({
					success: false,
					message:"No available session"
			});
	});

function requireRole(role) {
    return function(req, res, next) {
    	console(req.session.user.role);
        if(req.session.user && role.indexOf(req.session.user.role) > -1)
            next();
        else
            res.json({
            	success: false,
            	message: "Permission denied"
            });
    }
}

module.exports = router;