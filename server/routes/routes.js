var express = require('express');
var router = express.Router();

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'JADE-Bootstrap' });
// });

router.get(['/assets/*', '/css/*', '/js/*'], function(req, res, next){
	return next();
});

router.get('*', function(req, res, next){
	return next();
	
	// if(req.app.get('initialized') == true) {
	// 	//console.log('App initialized, redirecting');
	// 	if(req.url.substring(0, 6) == '/setup'){
	// 		res.redirect('/');
	// 	}
	// 	else{
	// 		return next();
	// 	}
	// } else {
	// 	if(req.url.substring(0, 6) != '/setup' && req.url.substring(0, 7) != '/assets' && req.url.substring(0, 5) != '/api'){
	// 	  	console.log('Redirecting to setup');
	// 	    res.redirect('/setup');
	// 	}
	// 	else{
	// 		return next();
	// 	}
	// }
});

// router.get('/setup', function(req,res){
// 	res.render('setup');
// });

// router.get('/dashboard', requireRole('admin'), function(req, res){
// 	res.render('dashboard');
// });

// router.get('/signin', function(req, res){
// 	res.render('signin');
// });

// router.get('/signup', function(req, res){
// 	res.render('signup');
// });

// // router.get('/', function(req, res){
// // 	res.render('index');
// // });

// router.get('/template/:selectedTemplate', function (req, res) {
//     res.render('bootstrap3-templates/' + req.params.selectedTemplate, {template: req.params.selectedTemplate});
// });

// function requireRole(role) {
//     return function(req, res, next) {
//     	console.log(req.user);

//         if(req.user && req.user.role.indexOf(role) > -1)
//             next();
//         else
//             res.redirect('/signin');
//     }
// }

module.exports = router;