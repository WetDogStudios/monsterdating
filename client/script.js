const PARTS = [ 'body', 'eyes', 'nose', 'mouth' ];
const NUMOPTIONS = 4;

function init () {
    for ( let i = 0; i < PARTS.length; i ++ ) {
        let partName = PARTS[i];
        setRandom( partName );
        hammerize( partName );
    };
}

function setRandom ( part ) {
    const randomNum = Math.floor(Math.random() * ( NUMOPTIONS - 1 + 1 )) + 1;
    setSelection( part, randomNum );
}

function hammerize ( partName ) {
    // hammerjs is a js library that handles touch events
    // make radiogroup into hammer element
    let hamm = new Hammer( document.getElementById( partName ) );

    // listen to events...
    hamm.on( "swipeleft tap", function( ev ) {
        cycle( 'left', ev );
    });

    hamm.on( "swiperight", function( ev ) {
        cycle( 'right', ev );
    });

    function cycle ( direction, ev ) {
        let newValue = getNewSelection( partName, direction );
        let ele = ev.target;

        ele.classList.add( 'fly-' + direction );
        setTimeout( function () {
            setSelection( partName, newValue );
            ele.classList.remove( 'fly-' + direction );
        }, 100 );
    }

    function getNewSelection ( partName, direction ) {
        const current = parseInt( getSelected( partName ).value );
        let newValue = direction == 'left' ? current - 1 : current + 1;

        if ( newValue < 1 ) { newValue = NUMOPTIONS; }
        if ( newValue > NUMOPTIONS ) { newValue = 1; }

        return newValue;
    }
}

// get and set the selected part
function getSelected ( part ) {
    return document.querySelector( 'input[name="'+ part + '"]:checked' );
}

function setSelection ( part, newValue ) {
    // clear current selection
    const currentSelection = getSelected( part );
    if ( currentSelection ) {
        currentSelection.removeAttribute( 'checked' );
        currentSelection.parentNode.classList.remove( 'selected' );
    }

    const partOptions = document.querySelectorAll( 'input[name="'+ part + '"]' );
    let newI = Object.keys( partOptions ).find( ( i ) => {
        return partOptions[i].value == newValue;
    });
    let newSelection = partOptions[newI];

    newSelection.setAttribute( 'checked', true );
    newSelection.parentNode.classList.add( 'selected' );
}

// send the selected data to our server
function monsterMake () {
    event.preventDefault();

    const xhr = new XMLHttpRequest();
    xhr.open( 'POST', '/monster' );

    const data = new FormData();
    for ( let i = 0; i < PARTS.length; i ++ ) {
        let partName = PARTS[i];
        addData( partName );
    };
    xhr.send( data );

    xhr.addEventListener( 'load', function ( e ) {
        const lucky = JSON.parse( xhr.responseText );
        document.getElementById( 'container' ).classList.add( 'hide' );
        document.getElementById( 'luckyNumber' ).classList.remove( 'hide' );
        document.getElementById( 'lucky' ).innerHTML = lucky.number;
    }, false );

    function addData ( part ) {
        const selected = getSelected( part ).value;
        data.append( part, selected );
    }
}

init();
