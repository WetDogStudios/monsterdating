// from https://www.sitepoint.com/build-javascript-countdown-timer-no-dependencies/

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
        // send request to the server
        const xhr = new XMLHttpRequest();
        xhr.open( 'POST', '/timesup' );

        const data = new FormData();
        data.append( 'drawing', true );
        xhr.send( data );

        xhr.addEventListener( 'load', function ( e ) {
            const response = JSON.parse( xhr.responseText );
            console.log( response );
        }, false );

      clearInterval( timeinterval );
      setTimeout( function () {
          minutesSpan.innerHTML = ('3');
          secondsSpan.innerHTML = ('00');
          clock.classList.add( 'flash' );
      }, 5000 );

      // set timer to restart
      setTimeout( function () {
        clock.classList.remove( 'flash' );
        start();
    }, 15000 );

    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);

}

function start () {
    var deadline = new Date(Date.parse(new Date()) + 3 * 60 * 1000);
    initializeClock( 'time', deadline );
}

start();
